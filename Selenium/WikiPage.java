package qaTraining;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WikiPage {

	public static void main(String[] args) {
		//Chrome
		System.setProperty("webdriver.chrome.driver", "C:/Desarrollo/WebDrivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
				
		try {
		driver.get("https://es.wikipedia.org/wiki/Wikipedia:Portada");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		String title = driver.getTitle();
		System.out.println("Default title: "+title);
		
		String searchKeyword = "selenium";
		
		WebElement keyword = driver.findElement(By.xpath("//*[@id=\"searchInput\"]"));
		keyword.sendKeys(searchKeyword);
		WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"searchButton\"]"));
		searchButton.click();
		
		String searchTitle = driver.getTitle();
		System.out.println("Search title: "+searchTitle);
		
		if(searchKeyword.concat(" - ").concat(title).equalsIgnoreCase(searchTitle))
			System.out.println("search title contains search keyword");
		else
			System.out.println("search title doesn't contain search keyword");
		Thread.sleep(10000);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		} finally {
		   driver.quit();
		}

	}

}
