package qaTraining;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ToolsQAPage {

	public static void main(String[] args) {
		//Chrome
		System.setProperty("webdriver.chrome.driver", "C:/Desarrollo/WebDrivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
				
		try {
		driver.get("https://toolsqa.com");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		String title = driver.getTitle();
		System.out.println("Default title: "+title);
		
		String searchKeyword = "selenium";
		
		WebElement keyword = driver.findElement(By.xpath("/html/body/header/nav/div/div/div[3]/div/div[2]/form/input"));
		keyword.sendKeys(searchKeyword);
		keyword.sendKeys(Keys.ENTER);
		
		Thread.sleep(2000);
		String staleElement = "How to avoid StaleElementReferenceException?";
		WebElement element = driver.findElement(By.xpath("//*[contains(text(), '"+staleElement+"')]"));
		
		if(element.getText().contains(staleElement))
			System.out.println("found element");
		else
			System.out.println("Element is not found");
		
		element.click();
		Thread.sleep(30000);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		} finally {
			driver.quit();
		}
		

	}

}
