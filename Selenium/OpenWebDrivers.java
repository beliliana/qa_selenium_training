package qaTraining;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class OpenWebDrivers {

	public static void main(String[] args) {
		
		//Chrome
		System.setProperty("webdriver.chrome.driver", "C:/Desarrollo/WebDrivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/login");
		driver.manage().window().maximize();
		driver.quit();
	
		
		//Firefox
		System.setProperty("webdriver.gecko.driver", "C:/Desarrollo/WebDrivers/geckodriver.exe");
		WebDriver driver3 = new FirefoxDriver();
		driver3.get("https://demoqa.com/login");
		driver3.manage().window().maximize();
		driver3.quit();


	}

}
