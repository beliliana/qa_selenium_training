package qaTraining;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitTime {
	static final String loginPage = "https://demoqa.com/login";
	static final String switchPage = "https://demoqa.com/browser-windows";

	public static void main(String[] args) {
		try {
			System.setProperty("webdriver.chrome.driver", "C:/Desarrollo/WebDrivers/chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
		    Actions actions = new Actions(driver);
		    //Implicit wait
		    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		    
		    //logIn
			driver.get(loginPage);
			driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("lilianaperez_10");
			driver.findElement(By.xpath("//input[@id='password']")).sendKeys("a1B2TR.5@");
			WebElement logIn = driver.findElement(By.xpath("//button[@id='login']"));
			actions.moveToElement(logIn).click().build().perform();
			
			
			driver.get(switchPage);
			 //Explicit wait
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='tabButton']")));
			//First page
			String parentWindowHandle = driver.getWindowHandle();
			System.out.println("Parent: "+parentWindowHandle);
			driver.findElement(By.xpath("//button[@id='tabButton']")).click();
			
			//Fluent wait
			Wait<WebDriver> waitFluent = new FluentWait<WebDriver>(driver)
					.withTimeout(Duration.ofSeconds(40))
					.pollingEvery(Duration.ofSeconds(10))
					.ignoring(Exception.class);
			
			waitFluent.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='windowButton']")));
			
			driver.findElement(By.xpath("//button[@id='windowButton']")).click();
			Set<String> s = driver.getWindowHandles();
			
			Iterator<String> iterator= s.iterator();
			String childWindow;
			
			while(iterator.hasNext()) {
				childWindow = iterator.next();
				if(!parentWindowHandle.equals(childWindow)) { 
					System.out.println("-------------------------------------------\nchildWindow: "+childWindow);
					driver.switchTo().window(childWindow);
						System.out.println("URL: "+driver.getCurrentUrl());
						System.out.println("Text: "+driver.findElement(By.tagName("h1")).getText());
				}
				Thread.sleep(5000);
			}
			driver.quit();
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
