package qaTraining;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class QAPageWebLocators {
	public static void main(String[] args){
		try {
		//Chrome
		System.setProperty("webdriver.chrome.driver", "C:/Desarrollo/WebDrivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/login");
		driver.manage().window().maximize();
		
		//Retrieving web page title
		String title = driver.getTitle();
		System.out.println("The page title is : " +title);
		
		//Locators
		
		//Locator by xpath
		WebElement uName = driver.findElement(By.xpath("//*[@id='userName']"));
		WebElement pswd = driver.findElement(By.xpath("//*[@id='password']"));
		WebElement loginBtn = driver.findElement(By.xpath("//*[@id='login']"));
		
		//Peforming actions on web elements
		uName.sendKeys("testuser");
		pswd.sendKeys("Password@123");
		loginBtn.click();
		
		
		//Locator by Id
		
		driver.get("https://demoqa.com/automation-practice-form");
		WebElement firstName = driver.findElement(By.id("firstName"));
		WebElement lastName = driver.findElement(By.id("lastName"));
		firstName.sendKeys("Liliana");
		lastName.sendKeys("Perez");
		
		//Locator by Name
		List<WebElement> optionButtons = driver.findElements(By.name("group-header"));
		
		for(WebElement option: optionButtons)
			System.out.println(option.getText());
		
		//Locator by TagName
		List<WebElement> inputs= driver.findElements(By.tagName("input"));
		for(WebElement input: inputs)
			System.out.println(input.getText());
		
		//Locator by Class
		List<WebElement> menuOptions = driver.findElements(By.name("header-text"));
		for(WebElement option: menuOptions)
			System.out.println(option.getText());
		
		//Locator by LinkText
		driver.get("https://demoqa.com/books");
		WebElement linkJS = driver.findElement(By.linkText("Learning JavaScript Design Patterns"));
		linkJS.click();
		Thread.sleep(5000);
		
		//Locator by PartialLink
		driver.get("https://demoqa.com/books");
		WebElement partialLinkJS = driver.findElement(By.partialLinkText("Learning JavaScript"));
		partialLinkJS.click();;
		
		//Locator by CSSSelector
		WebElement searchButton = driver.findElement(By.cssSelector("label[id='userName-value']"));
		System.out.println("ISBN: "+searchButton.getText());
		
	
	//	driver.quit();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
